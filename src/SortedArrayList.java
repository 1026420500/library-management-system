import java.util.ArrayList;



public class SortedArrayList<E> extends ArrayList<E> {


    public int insert(E e){
        this.add(e);
        if (e instanceof Book){
            Book book = (Book) e;
            SortedArrayList<Book> list = (SortedArrayList<Book>) this;
            for(int i = list.size()-1; i > 0; i--){
                for(int j = i -1; j > 0; j--){
                    String foreSurName = list.get(j).getAuthorName().split(",")[1];
                    String handSurName = list.get(i).getAuthorName().split(",")[1];
                    if(foreSurName.compareTo(handSurName) > 0){
                        Book temp = list.get(i);
                        list.set(i,list.get(j));
                        list.set(j,temp);
                    }
                }
            }
        }

        if(e instanceof User){
            User user = (User) e;
            SortedArrayList<User> list = (SortedArrayList<User>) this;
            for(int i = list.size()-1; i > 0; i--){
                for(int j = i -1; j > 0; j--){
                    String foreSurName = list.get(j).getSurName();
                    String handSurName = list.get(i).getSurName();

                    if(foreSurName.compareTo(handSurName) > 0){
                        User temp = list.get(i);
                        list.set(i,list.get(j));
                        list.set(j,temp);
                    }
                }
            }
        }


        return 1;
    }
}