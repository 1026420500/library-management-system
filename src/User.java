

public class User implements Comparable<User>{
    private String firstName;
    private String surName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    @Override
    public int compareTo(User o) {
        int result = compareSurName(this.surName, o.surName);
        if(result != 0){
            return result;
        }

        return compareFirstName(this.firstName,o.firstName);
    }
    /**
     * @param surName1
     * @param surName2
     * @return
     */
    public static int compareSurName(String surName1, String surName2) {
        return ( surName1.compareTo(surName2) > 0 ? 1 :
                (surName1.compareTo(surName2) == 0 ? 0 : -1));
    }
    /**
     * @param firstName1
     * @param firstName2
     * @return
     */
    public static int compareFirstName(String firstName1,String firstName2){
        return (firstName1.compareTo(firstName2) > 0 ? 1 :
                (firstName1.compareTo(firstName2) == 0 ? 0 : -1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        return surName != null ? surName.equals(user.surName) : user.surName == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (surName != null ? surName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return
                 firstName + ' ' +
                  surName
              ;
    }
}