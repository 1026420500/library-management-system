import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class Controller {
    public void showAllBooks(SortedArrayList<Book> bookList) {



        System.out.println("排序前");
        System.out.println(bookList);
        List<String[]> ls = new ArrayList<>();
        String[] tableHeader = new String[3];
        tableHeader[0] = "Title";
        tableHeader[1] = "AuthorName";
        tableHeader[2] = "LendStatus";
        int bookCountOnLib = 0;
        for (int i = 0; i < bookList.size(); i++) {
            if(bookList.get(i).getLendStatus().equals(0)){
                bookCountOnLib++;
            }
            String[] rowN = new String[3];
            rowN[0] = bookList.get(i).getTitle();
            rowN[1] = bookList.get(i).getAuthorName().split(",")[0] +" "+bookList.get(i).getAuthorName().split(",")[1];
            rowN[2] = bookList.get(i).getLendStatus() == 0? "onLibrary" :" notOnLibrary";
            ls.add(rowN);
            // System.out.println(bookList.get(i).getAuthorName().replaceAll(","," "));
        }
        ConsoleTable ct = new ConsoleTable(ls, tableHeader, 3, true);
        ct.print();
        System.out.println("图书库存为" + bookCountOnLib);

        System.out.println("排序后");
        Collections.sort(bookList);
        System.out.println(bookList);
        List<String[]> lshou = new ArrayList<>();
        String[] tableHeaderhou = new String[3];
        tableHeaderhou[0] = "Title";
        tableHeaderhou[1] = "AuthorName";
        tableHeaderhou[2] = "LendStatus";
        for (int i = 0; i < bookList.size(); i++) {
            String[] rowN = new String[3];
            rowN[0] = bookList.get(i).getTitle();
            rowN[1] = bookList.get(i).getAuthorName().split(",")[0] +" "+ bookList.get(i).getAuthorName().split(",")[1];
            rowN[2] = bookList.get(i).getLendStatus()== 0? "onLibrary" :" notOnLibrary";
            lshou.add(rowN);
            //System.out.println(bookList.get(i).getAuthorName().replaceAll(","," "));
        }
        ConsoleTable cthou = new ConsoleTable(lshou, tableHeaderhou, 3, true);
        cthou.print();
    }

    public void showAllUsers(SortedArrayList<User> userList) {

        System.out.println("排序前");
        System.out.println(userList);
        for (int i = 0; i < userList.size(); i++) {
            System.out.println(userList.get(i).getFirstName() + " " + userList.get(i).getSurName());

        }
        System.out.println("排序后");
        Collections.sort(userList);
        System.out.println(userList);
        for (int i = 0; i < userList.size(); i++) {

            System.out.println(userList.get(i).getFirstName() + " " + userList.get(i).getSurName());

        }
    }

    public int isBookExist(String bookName, SortedArrayList<Book> bookList) {
        int r = 0;

        for (int i = 0; i < bookList.size(); i++) {
            if (!"".equals(bookName) && bookList.get(i).getTitle().equals(bookName)) {
                r = r + 1;

            }

        }
        return r;
    }

    public void showAllLendInfo(SortedArrayList<LendInfo> lendInfoList) {

        int k = 0;
        List<String[]> tableContent = new ArrayList<>();
        String[] tableHeader = new String[4];
        tableHeader[0] = "UserName";
        tableHeader[1] = "BookTitle";
        tableHeader[2] = "AuthorName";
        tableHeader[3] = "LendStatus";
        for (int i = 0; i < lendInfoList.size(); i++) {
            ++k;
            String[] tableRow = new String[4];
            tableRow[0] = lendInfoList.get(i).getUser().toString();
            tableRow[1] = lendInfoList.get(i).getBook().getTitle();
            tableRow[2] = lendInfoList.get(i).getBook().getAuthorName().split(",")[0]+" "+lendInfoList.get(i).getBook().getAuthorName().split(",")[1];
            tableRow[3] = lendInfoList.get(i).getBook().getLendStatus() == 1 ? "onLoan" : "notOnLoan";
            tableContent.add(tableRow);
            //  System.out.println(lendInfoList.get(i));
        }
        ConsoleTable ct = new ConsoleTable(tableContent, tableHeader, 4, true);
        ct.print();
        if (k == 0) {
            System.out.println("暂无借阅信息");
        }

    }


}