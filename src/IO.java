import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;



public class IO {
    private static SortedArrayList<User> userList;
    private static SortedArrayList<Book> bookList;
    private static SortedArrayList<LendInfo> lendInfoList;
    public static SortedArrayList<User> getUserList() {
        return userList;
    }

    public static void setUserList(SortedArrayList<User> userList) {
        IO.userList = userList;
    }

    public static SortedArrayList<Book> getBookList() {
        return bookList;
    }

    public static void setBookList(SortedArrayList<Book> bookList) {
        IO.bookList = bookList;
    }

    public static SortedArrayList<LendInfo> getLendInfoList() {
        return lendInfoList;
    }

    public static void setLendInfoList(SortedArrayList<LendInfo> lendInfoList) {
        IO.lendInfoList = lendInfoList;
    }

    public static void main(String[] args) throws FileNotFoundException {
        readToListFile();//将data.txt文件里的内容分别读进booklist.txt和userlist.txt;
        init();//实例化三个SortedArrayList<>集合对象,并给其中两个(即书本与用户列表)初始化.
        Scanner sc = new Scanner(System.in);
        System.out.println("欢迎使用图书管理系统");
     /*
     *以下for循环纯属摆看的
     */
        for(int i = 0; i < 5; i ++) {
        if(i != 4) {
            System.out.print("*");
        }else {
            System.out.println("*");
        }
        try{
            Thread.sleep(100);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
        System.out.println("系统加载完成");
        librarySystem(sc);
}

    public static void readToListFile() throws FileNotFoundException {
        Scanner inFile = new Scanner(new FileReader("D:\\cs8012\\data.txt"));
        int count = 0;
        StringBuilder bookListBuilder = new StringBuilder();
        StringBuilder userListBuilder = new StringBuilder();
        while (inFile.hasNext()) {
            ++count;
            if (count >= 1 && count <= 11) {
                //System.out.println("写入到booklist里去");
                PrintWriter outFile = new PrintWriter("D:\\workspace\\coursework\\booklist.txt");
                bookListBuilder.append(inFile.nextLine() + "\r\n");
                if (count == 11) {
                    outFile.write(bookListBuilder.toString());
                }
                outFile.flush();
            } else {
                // System.out.println("写入到userlist里去");
                PrintWriter outFile = new PrintWriter("D:\\workspace\\coursework\\userlist.txt");
                userListBuilder.append(inFile.nextLine() + "\r\n");
                if (count == 16) {
                    outFile.write(userListBuilder.toString());
                }
                outFile.flush();
            }
        }

    }

    public static void init ( ) throws FileNotFoundException {
        userList = new SortedArrayList<>();
        bookList = new SortedArrayList<>();
        lendInfoList = new SortedArrayList<>();
        insantiateUserList(userList);
        insantiateBookList(bookList);

    }

    static void insantiateUserList(SortedArrayList<User> userList) throws FileNotFoundException {
        Scanner inFile = new Scanner(new FileReader("D:\\workspace\\coursework\\userlist.txt"));
        inFile.nextLine();//执行这行代码是为了跳过userlist.txt文件中的第一行的数字
        while (inFile.hasNext()){
            String name = inFile.nextLine();
            String firstName = name.split(",")[0];
            String surName = name.split(",")[1];
            User user = new User();
            user.setFirstName(firstName);
            user.setSurName(surName);
            userList.add(user);
        }
    }


    static void insantiateBookList(SortedArrayList<Book> bookList) throws FileNotFoundException {
        Scanner inFile = new Scanner(new FileReader("D:\\workspace\\coursework\\booklist.txt"));
        inFile.nextLine();
        int count = 0;
        while (inFile.hasNext()){
            ++count;
            if(count % 2 != 0){
                String title = inFile.nextLine();
                String authorName = inFile.nextLine();
                Book book = new Book();
                book.setTitle(title);
                book.setAuthorName(authorName);
                book.setLendStatus(0);
                bookList.add(book);
            }
        }

    }


    public static void librarySystem(Scanner s) throws FileNotFoundException {

        Controller controller = new Controller();
        boolean flag = true;
        while (flag) {
            System.out.println("请选择操作指令：1.展示所有图书信息; 2.展示所有用户信息; 3.借阅图书,更新图书馆库存; 4.归还图书,更新图书馆库存; 5.查看图书借阅信息; 6.退出系统");
            switch (s.next()) {
                case "1":
                    //查询所有的图书信息
                    controller.showAllBooks(bookList);
                    break;

                case "2":
                    //查询所有的用户信息
                    controller.showAllUsers(userList);
                    break;
                case "3":
                    if( Util.checkUserValidAndLendBook() > 0){
                        System.out.println("借书成功");
                    }else {
                        System.out.println("借书失败");
                    }
                    break;
                case "4":
                    //检查用户合法性
                    if(Util.checkUserValid() == 1){
                        System.out.println("用户存在");
                    };
                    System.out.println("请输入想要归还的图书的作者姓氏");
                    String sur = s.next();
                    Book book = Util.findBookByAuthorSurNameInLend(sur);
                    if(book != null && book.getLendStatus() == 1){
                        //这个用户确实借书了,准备还书操作

                        Util.updateLendInfo(book);
                        book.setLendStatus(0);//书本自身变为可借状态
                        //bookList.add(book);


                        System.out.println("图书归还成功");
                    }else{
                        System.out.println("该用户并没有借这本书籍");
                    }
                    break;
                case "5":
                    //查询图书借阅信息
                    controller.showAllLendInfo(lendInfoList);
                    break;
                case "6":
                    Util.writeLendInfoListToFile();

                    System.out.println("欢迎再次使用");
                    flag = false;
                    break;
                default:
                    System.out.println("输入有误，请重新输入数字指令");
                    break;

            }
        }
    }


}


