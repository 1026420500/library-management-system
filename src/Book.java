

public class Book implements Comparable<Book> {
    private String title;
    private String authorName;
    private Integer lendStatus;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLendStatus() {
        return lendStatus;
    }

    public void setLendStatus(Integer lendStatus) {
        this.lendStatus = lendStatus;
    }

    @Override
    public int compareTo(Book o) {
        String authorSurName1 = authorName.split(",")[1];
        String authorSurName2 = o.authorName.split(",")[1];
        return authorSurName1.compareTo(authorSurName2) > 0 ? 1 :
                (authorSurName1.compareTo(authorSurName2) == 0 ? 0 : -1);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", authorName='" + authorName + '\'' +
                ", lendStatus=" + lendStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (authorName != null ? !authorName.equals(book.authorName) : book.authorName != null) return false;
        return lendStatus != null ? lendStatus.equals(book.lendStatus) : book.lendStatus == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (lendStatus != null ? lendStatus.hashCode() : 0);
        return result;
    }


}