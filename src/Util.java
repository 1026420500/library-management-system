import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Util {
    private static SortedArrayList<User> userList = IO.getUserList();
    private static SortedArrayList<Book> bookList = IO.getBookList();
    private static SortedArrayList<LendInfo> lendInfoList = IO.getLendInfoList();
    private static Scanner scanner = new Scanner(System.in);
    public static int judgeAuthorExist(String authorfisrName) {
        int r = 0;
        for(Book book : bookList){
            String firstName = book.getAuthorName().split(",")[1];
            if(!"".equals(authorfisrName) && firstName.equals(authorfisrName)){
                r = 1;
            }
        }
        return r;
    }


    public static int judgeBookExist(String bookName) {
        int r = 0;
        if(!"".equals(bookName) && bookName != null){
            for(Book book : bookList){
                if(book.getTitle().equals(bookName)){

                    r = r + 1;
                }
            }
        }
        if(r == 0){
            System.out.println("图书馆不存在这本书,请重新输入");
        }

        return r;
    }

    public static int oparatorBookInLibrary(User user) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        while(flag){
            System.out.println("请输入要借的图书名");
            String bookName = sc.nextLine();
            //这是书存在的情况
            if(Util.judgeBookExist(bookName)== 1){
                //是否被借走了
                if(isLendedOrNot(bookName) == 1){
                    lendedThenPrintToFile(user,bookName);
                    return 0;
                }

                Book book = null;
                for(Book bk : bookList ){
                    if(bk.getTitle().equals(bookName)){
                        book = bk;
                    }
                }
                lendBook(user,book,1);
                return 1;
            }else if(Util.judgeBookExist(bookName) == 2){
                System.out.println("该图书名在图书馆有两本,请再输入作者姓氏查找");
                String authorfisrtName = sc.next();
                if(Util.judgeAuthorExist(authorfisrtName) != 1){
                    System.out.println("输入作者名未找到,请重新输入");
                    return 0;
                }
                if(isLendedOrNot(bookName) == 1){
                    lendedThenPrintToFile(user,bookName);
                    return 0;
                }
                flag = false;
                Book book = findBookByAuthor(authorfisrtName);
                //找到该作者的书了
                lendBook(user,book,1);
                return 1;
            }
        }

        return 0;
    }
    public static void lendedThenPrintToFile(User user,String bookName) throws FileNotFoundException {
        //书被借走的情况
        System.out.println("您所查找的图书已被借出,打印通知书本持有人尽快还书的便条将产生");
        //输出一句话到一个文件里，通知该书持有人，有人需要这本书，请尽快归还
        PrintWriter outFile = new PrintWriter("D:\\workspace\\coursework\\note.txt");
        //根据book对象去lendlist里面找(进到这里这本书的lendstatus已经是1),看借书的人是谁.
        User lender = Util.findUserInLendListByBook(bookName);
        StringBuilder waitToPrint = new StringBuilder();
        waitToPrint.append(bookName);
        waitToPrint.append("书本的持有人");
        waitToPrint.append(lender.getFirstName()+" "+lender.getSurName());
        waitToPrint.append("你好,该书正在被");
        waitToPrint.append(user.getFirstName()+" "+user.getSurName());
        waitToPrint.append("需要,请及时归还");
        outFile.write(waitToPrint.toString());
        outFile.flush();
    }

    private static int isLendedOrNot(String bookName) {
        int r = 0;
        for(LendInfo li : lendInfoList){

            if(li.getBook().getTitle().equals(bookName)){
                if(li.getBook().getLendStatus() == 1){

                r = 1;
                }
            }
        }
        return r;
    }

    public static User findUserInLendListByBook(String bookName){

        User user = null;
        for(LendInfo li : lendInfoList){
            if(li.getBook().getTitle().equals(bookName)){
                user = li.getUser();
            }
        }
        return user;
    }

    public static void lendBook(User user,Book book,Integer lendNumber) throws FileNotFoundException {


        for(int i = 0; i < bookList.size(); i++){
            if( bookList.get(i).getAuthorName().equals(book.getAuthorName())){

                    //现在可以开始借书了
                    //判断要借的书是否已经在借阅信息表中,没有在则添加,有在则修改借阅状态
                    for(int j = 0; j < lendInfoList.size(); j++){
                        if(lendInfoList.get(j).getBook().getAuthorName().equals(bookList.get(i).getAuthorName())){
                            //只需要修改借阅信息表里的借阅状态就行
                            lendInfoList.get(j).getBook().setLendStatus(1);
                            return;
                        }
                    }

                    bookList.get(i).setLendStatus(1);
                    LendInfo lendInfo = new LendInfo();
                    lendInfo.setUser(user);
                    lendInfo.setBook(bookList.get(i));
                    lendInfo.setLendNumber(lendNumber);
                    lendInfoList.add(lendInfo);


                    //图书列表里移除这本书更新库存
                   // bookList.remove(i);
            }

        }
    }


    public static Book findBookByAuthor(String authorName) {
        Book book = null;
        for(Book bk : bookList) {
            String authorFirstName = bk.getAuthorName().split(",")[1];
            if(authorFirstName.equals(authorName)){
                book = bk;
            }
        }
        return  book;
    }

    public static int checkUserValidAndLendBook() throws FileNotFoundException {

        System.out.println("请输入用户的fisrtName");
        String firstName = scanner.next();
        System.out.println("请输入用户的surName");
        String surName = scanner.next();
        User user = new User();
        user.setFirstName(firstName);
        user.setSurName(surName);
        if(!userList.contains(user)){
            System.out.println("无效用户,未在图书馆用户列表中未找到,请重新输入");
           return 0;
        }
        //检查用户借书数量是否超过三本
        int lendCount = 0;
        for(int j = 0; j < lendInfoList.size(); j++){
            if(lendInfoList.get(j).getUser().equals(user) && lendInfoList.get(j).getBook().getLendStatus() == 1){
                lendCount += lendCount + lendInfoList.get(j).getLendNumber();
            }
        }
        if(lendCount > 3){
            System.out.println("当前用户借书数量已达最大数目3,请归还后再借");
            return 0;
        }

        return Util.oparatorBookInLibrary(user);
    }
    public static int checkUserValid(){
        while (true){
            System.out.println("请输入用户的fisrtName");
            String firstName = scanner.next();
            System.out.println("请输入用户的surName");
            String surName = scanner.next();
            User user = new User();
            user.setFirstName(firstName);
            user.setSurName(surName);
            if(!userList.contains(user)){
                System.out.println("无效用户,未在图书馆用户列表中未找到,请重新输入");

            }else {
                break;
            }

        }

        return 1;
    }

    public static Book findBookByAuthorSurNameInLend(String surName) {
        Book book = null;
        if(!"".equals(surName) && surName != null){
            for(LendInfo li : lendInfoList) {
                String sur = li.getBook().getAuthorName().split(",")[1];
                if(sur.equals(surName)){
                    book = li.getBook();
                }
            }
        }
        return book;
    }

    public static void updateLendInfo(Book book) {

        for(int i = 0; i < lendInfoList.size(); i++){
            if(lendInfoList.get(i).getBook().getTitle().equals(book.getTitle())){
                //lendInfoList.remove(i);
                lendInfoList.get(i).getBook().setLendStatus(0);
            }
        }
    }


    public static void writeLendInfoListToFile() throws FileNotFoundException {
        PrintWriter outFile = new PrintWriter("D:\\workspace\\coursework\\lendInfoList.txt");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < lendInfoList.size(); i++){
            sb.append(lendInfoList.get(i)+"\r\n");

        }
        outFile.write(sb.toString());
        outFile.flush();

    }
}