
public class LendInfo {
    private User user;
    private Book book;
    private Integer lendNumber;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getLendNumber() {
        return lendNumber;
    }

    public void setLendNumber(Integer lendNumber) {
        this.lendNumber = lendNumber;
    }

    @Override
    public String toString() {
        return "LendInfo{" +
                "user=" + user +
                ", book=" + book +
                ", lendNumber=" + lendNumber +
                '}';
    }
}